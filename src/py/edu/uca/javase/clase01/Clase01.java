package py.edu.uca.javase.clase01;

import java.util.Scanner;

public class Clase01 {

	//Esto es un comentario de un linea
	
	/*
		Esto es un comentario
		que tiene multiples
		lineas!
	*/
	
	/**
	 * Esto es un comentario para JavaDoc
	 * @author Victor Cajes
	 */
	
	//Esto es una constante entera global
	public static final int CONSTANTE_GLOBAL = 10;
	
	enum Size { SMALL, MEDIUM, LARGE, EXTRA_LARGE };

	 
	public static void main(String args[]){
		short numero = 25;
		String cadena = "Esto es una variable alfanumerica";
		int resultadoFuncionSuma = suma(40, 30);
		double flotante = 50.87878;
		int enteroCasteado = (int) flotante;
		
		Size size = Size.EXTRA_LARGE;
		
		System.out.println("Mi primer programa en Java!");
		System.out.println("La constante global es: " + CONSTANTE_GLOBAL);
		System.out.println("La variable 'numero' es: " + numero);
		System.out.println("La variable 'cadena' es: " + cadena);
		System.out.println("El resultado de la suma (40+30) es: " + resultadoFuncionSuma);
		System.out.println("La variable double es: " + flotante);
		System.out.println("La variable double casteado a entero es: " + enteroCasteado);
		
		//asi verificamos que la enumeracion sea igual a algun valor de la lista de valores posibles
		if(size.equals(Size.EXTRA_LARGE)){
			System.out.println("La variable enumaracion es: " + size.toString());
		}
		
		//Entrada de informacion
		System.out.print("Ingrese su nombre por favor:\t");
		Scanner in = new Scanner(System.in); //abrimos un lector de datos desde el teclado
		String nombre = in.nextLine(); //guardamos el texto leido en la variable nombre
		System.out.println("El nombre ingresado fue:\t" + nombre);
		
		
		//tambien podemos leer un entero
		System.out.print("Ingrese su edad en años por favor:\t");
		int edad = in.nextInt();
		System.out.println("La edad entera ingresada fue:\t" + edad);
		
		in.close(); //cerramos el lector de datos
		
		//Impresion formateada de datos
		System.out.printf("Un double con dos decimales es: %.2f \n", flotante);
		System.out.printf("Hola: %s. El año que viene tendrás la edad de %d \n", nombre, edad+1);
	}
	
	public static int suma(int num1, int num2){
		return (num1 + num2);
	}
}
