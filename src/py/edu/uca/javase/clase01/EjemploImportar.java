package py.edu.uca.javase.clase01;

import java.util.Date;
import py.edu.uca.javase.clase02.Pelota;

public class EjemploImportar {

	public static void main(String args[]){
		Pelota pelota = new Pelota("roja");
		System.out.println("La pelota es de color " + pelota.getColor());
		
		Date fechaHoy = new Date();
		System.out.println("La fecha de hoy es " + fechaHoy.toString());
	}
}

