package py.edu.uca.javase.ejercicios;
import java.util.Scanner;


public class Clase1EJ1 {

	static int varGlobal = 75;
	
	//Leer un numero y crear una funcion que calcule el factorial del numero
	//Luego imprimir el factorial de dicho numero
	public static void main(String[] args) {
		System.out.print("Ingrese el número para el factorial:\t");
		Scanner in = new Scanner(System.in);
		int numero = in.nextInt();
		
		if(numero < 0){
			System.out.println("No debes ingresar numeros negativos.");
			System.exit(0);
		}else{
			System.out.println("Se va a calcular el factorial de " + numero);
			int fact = calcularFactorial(numero);
			System.out.println("El factorial("+numero+") es: " + fact);
		}
		in.close();
	}
	
	static int calcularFactorial(int num){
		System.out.println("La funcion recibe el parametro num = " + num);
		int fact = 1;
		for(int i=1; i<=num; i++){
			fact = fact * i;
		}
		return fact;
	}
}
