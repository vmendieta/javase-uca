package py.edu.uca.javase.ejercicios;

public class Clase2EJ1 {

	public static void main(String args[]){
		Clase2EJ1 outer = new Clase2EJ1();
		Cuadrado cuadrado = outer.new Cuadrado(10.0);
		Rectangulo rectangulo = outer.new Rectangulo(5.0, 8.0);
		TrianguloRectangulo trirec = outer.new TrianguloRectangulo(5.0, 8.0);
		
		System.out.println(cuadrado.toString());
		System.out.println(rectangulo.toString());
		System.out.println(trirec.toString());
	}
	
	private class Cuadrado{
		private double lado;
		@SuppressWarnings("unused") public Cuadrado(){ this.lado = 0.0; }
		public Cuadrado(double lado){ this.lado = lado;}
		public double perimetro(){ return lado * 4; }
		public double area(){ return Math.pow(this.lado, 2.0); }
		public String toString(){ 
			return String.format(
				"Cuadrado - Lado: %.2f - Perimetro: %.2f - Area: %.2f",
				lado, perimetro(), area());
		}
	}
	
	private class Rectangulo{
		private double ancho;
		private double largo;
		@SuppressWarnings("unused") public Rectangulo(){ this.ancho = 0.0; this.largo = 0.0; }
		public Rectangulo(double ancho, double largo){ this.ancho = ancho; this.largo = largo; }
		public double perimetro(){ return 2*largo + 2*ancho; }
		public double area(){ return largo*ancho; }
		public String toString(){ 
			return String.format(
				"Rectangulo - Ancho: %.2f - Largo: %.2f - Perimetro: %.2f - Area: %.2f",
				ancho, largo, perimetro(), area());
		}
	}
	
	private class TrianguloRectangulo{
		private double cateto1;
		private double cateto2;
		@SuppressWarnings("unused") public TrianguloRectangulo(){ this.cateto1 = 0.0; this.cateto2 = 0.0; }
		public TrianguloRectangulo(double cateto1, double cateto2){
			this.cateto1 = cateto1; this.cateto2 = cateto2;
		}
		public double perimetro(){ return cateto1 + cateto2 + hipotenusa(); }
		public double area(){ return cateto1*cateto2/2.0; }
		public double hipotenusa(){ return Math.pow(cateto1*cateto1 + cateto2*cateto2, 0.5); }
		public String toString(){ 
			return String.format(
				"Rectangulo - Cateto1: %.2f - Cateto2: %.2f - Hipotenusa: %.2f - Perimetro: %.2f - Area: %.2f",
				cateto1, cateto2, hipotenusa(), perimetro(), area());
		}
	}
}
