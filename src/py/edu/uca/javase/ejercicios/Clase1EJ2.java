package py.edu.uca.javase.ejercicios;
import java.lang.Math;
import java.util.Scanner;

public class Clase1EJ2 {

	public static void main(String[] args) {
		double var = Double.parseDouble(args[0]);
		System.out.println("El coseno del primer parametro es: " + Math.cos(var));
		
		int tam = 3;
		int suma = 0;
		int[] vector = new int[tam];
		Scanner in = new Scanner(System.in);
		
		for(int i=0; i<vector.length; i++){
			vector[i] = in.nextInt();
			suma = suma + vector[i];
		}
		
		in.close();
		System.out.print("El promedio es: " + (suma/vector.length));
	}

}
