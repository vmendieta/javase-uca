package py.edu.uca.javase.clase03;

public abstract class FiguraGeometrica {

	private String nombre;
	
	public FiguraGeometrica(){
		this.nombre = "[NOMBRE NO ESTABLECIDO]";
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public void printDetails(){
		System.out.println(this.nombre + " - Área: " + this.area() + " - Perímetro: " + this.perimetro());
	}
	
	//Las subclases deben implementar este metodo
	public abstract double area();
	
	//Las subclases deben implementar este metodo
	public abstract double perimetro();
}

