package py.edu.uca.javase.clase03;

public class Gerente extends Empleado{

	private double bonus;
	
	public Gerente(String nombre, double salario, double bonus){
		super(nombre, salario); //esta linea es opcional, y llama al constructor de la superclase Empleado
		this.bonus = bonus;
	}
	
	protected void setBonus(double bonus){
		this.bonus = bonus;
	}
	
	protected double getBonus(){
		return this.bonus;
	}
	
	protected double getSalarioTotal(){
		return (this.salario + this.bonus) - (this.salario + this.bonus) * 0.09;
	}
}
