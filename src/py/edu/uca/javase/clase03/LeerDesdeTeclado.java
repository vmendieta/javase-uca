package py.edu.uca.javase.clase03;

public interface LeerDesdeTeclado {

	/*
	 * En las interfaces no se pueden definir variables como miembros de las clases
	 *
	 */
	//int variableNormal; //esto genera un error de compilacion
	
	public void solicitarValoresAlUsuario();
	
}
