package py.edu.uca.javase.clase02;

public class Pelota {

	private String color;
	public String marca;
	
	public static String variableEstatica;
	
	public Pelota(){
		color = "SIN_COLOR";
	}
	
	public Pelota(String color){
		this.color = color;
	}
	
	public void setColor(String nuevoColor){
		this.color = nuevoColor;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public static String obtenerValorEstatico(){
		return "prueba";
	}
}

