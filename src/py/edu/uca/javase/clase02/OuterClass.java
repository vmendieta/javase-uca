package py.edu.uca.javase.clase02;

public class OuterClass {

	private int numero;
	private InnerClass ic;
	
	public OuterClass(int numero){
		this.numero = numero;
		this.ic = new InnerClass("hola");
	}
	
	public void setNumero(int numero){
		this.numero = numero;
	}
	
	public int getNumero(){
		return this.numero;
	}
	
	protected class InnerClass{
		private String nombre;
		
		public InnerClass(String nuevoNombre){
			this.nombre = nuevoNombre;
		}
		
		public void setNombre(String nuevoNombre){
			this.nombre = nuevoNombre;
		}
		
		public String getNombre(){
			return this.nombre;
		}
	}
}

