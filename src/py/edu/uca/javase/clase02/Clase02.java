package py.edu.uca.javase.clase02;

public class Clase02 {

	public static void main(String args[]){
		Pelota pelota; //declaracion de un objeto de clase Pelota
		pelota = new Pelota(); //instanciamos la clase pelota usando el constructor por defecto
		System.out.println(pelota.getColor());
		
		Pelota pelota2 = new Pelota("roja"); //declaramos e instanciamos una clase Pelota usando el constructor que recibe color como parametro
		System.out.println(pelota2.getColor());
		
		//Variables estaticas
		pelota.variableEstatica = "valor1"; //el cambio ya es para la clase en si, no solo para instancias
		//tambien se puede hacer:   Pelota.variableEstatica = "valor1";
		System.out.println(Pelota.variableEstatica); //imprime: valor1
		System.out.println(pelota.variableEstatica); //imprime: valor1
		System.out.println(pelota2.variableEstatica); //imprime: valor1
		
		System.out.println(Pelota.obtenerValorEstatico()); //imprime: prueba
		
		//Para crear un inner class desde afuera
		OuterClass outer = new OuterClass(10);
		OuterClass.InnerClass inner = outer. new InnerClass("nom2");
		System.out.println("El nombre de la inner class es: " + inner.getNombre());
	}
}

